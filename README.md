# Install Deeploy

This repository is part of the [complete installation guide](https://docs.deeploy.ml/next/installation/intro) and contains detailed instructions how to install the Deeploy Software Stack on Kubernetes using [Helm](https://helm.sh/) for production use.

## Table of Contents

- [Install Deeploy](#install-deeploy)
  - [Table of Contents](#table-of-contents)
  - [Install the Deeploy Software Stack](#install-the-deeploy-software-stack)
  - [Step 1. Prepare helm installation](#step-1-prepare-helm-installation)
    - [Requirements](#requirements)
    - [Pod autoscaling](#pod-autoscaling)
  - [Step 2. Deeploy Helm chart](#step-2-deeploy-helm-chart)
    - [Values](#values)
  - [Update Deeploy](#update-deeploy)
  - [Uninstall Deeploy](#uninstall-deeploy)

## Install the Deeploy Software Stack

We assume that you now have the prerequisites and infrastructure ready as defined in [the installation guide](https://docs.deeploy.ml/next/installation/amazon-eks/eks-prereq). From here we continue deploying the Deeploy software stack with dependencies.

## Step 1. Prepare helm installation

Use the installation script `./scripts/install-requirements.sh` to install the software requirements for the recommended Deeploy installation.

### Requirements

| Name            | Version | URL                               |
| --------------- | ------- | --------------------------------- |
| Istio           | 1.21.6  | https://istio.io/docs/            |
| Knative Serving | 1.13.1  | https://knative.dev/docs/serving/ |
| Cert-manager    | 1.13.6  | https://cert-manager.io/docs/     |

The following pods should be running successfully (status is `Running`) after the installation:

- Run `kubectl get pods -n istio-system` to check the Istio installation. Example output:

```
NAME                                   READY   STATUS    RESTARTS   AGE
istio-ingressgateway-7776dd578-5gkpl   1/1     Running   0          63s
istiod-56c76db8bc-sc5r5                1/1     Running   0          63s
```

- Run `kubectl get pods -n knative-serving` to check the Knative Serving installation. Example output:

```
NAME                                      READY   STATUS    RESTARTS   AGE
activator-67656dcbbb-8mftq                1/1     Running   0          97s
autoscaler-df6856b64-5h4lc                1/1     Running   0          97s
controller-788796f49d-4x6pm               1/1     Running   0          97s
domain-mapping-65f58c79dc-9cw6d           1/1     Running   0          97s
domainmapping-webhook-cc646465c-jnwbz     1/1     Running   0          97s
net-istio-controller-65685496f8-vskrq     1/1     Running   0          81s
net-istio-webhook-5588bc847f-xljlf        1/1     Running   0          81s
webhook-859796bc7-8n5g2                   1/1     Running   0          96s
```

- Run `kubectl get pods -n cert-manager` to check the Cert-manager installation. Example output:

```
NAME                                      READY   STATUS    RESTARTS   AGE
cert-manager-55658cdf68-t2mwz             1/1     Running   0          79s
cert-manager-cainjector-967788869-jm9w4   1/1     Running   0          79s
cert-manager-webhook-6668fbb57d-fzp5w     1/1     Running   0          79s
```

### Pod autoscaling

Deeploy uses pod autoscaling. The Kubernetes Metrics Server is necessary for pod autoscaling.
Most cloud providers pre-install the Metrics Server in your cluster, but some do not.

Validate that the `metrics-server` pod is running in the `kube-system` namespace. If not, install it.

- [AWS EKS Metrics Server Guide](https://docs.aws.amazon.com/eks/latest/userguide/metrics-server.html)
- [Digital Ocean Metrics Server Guide](https://www.digitalocean.com/community/tutorials/how-to-autoscale-your-workloads-on-digitalocean-kubernetes#step-3-%E2%80%94-installing-metrics-server)

## Step 2. Deeploy Helm chart

We advise to check https://artifacthub.io/packages/helm/deeploy-core/deeploy for the latest stable helm chart installation.
Deploy the Deeploy stack after adjusting the [values](https://artifacthub.io/packages/helm/deeploy-core/deeploy?modal=values). The resulting command is similar to:

```bash
helm repo add deeploy https://deeploy-charts.storage.googleapis.com/
helm repo update
helm install deeploy deeploy/deeploy -f values.yaml --namespace deeploy --create-namespace --version 1.45.1
```

### Values

For all available values in this Helm chart check [./helm/deeploy/values.yaml](https://artifacthub.io/packages/helm/deeploy-core/deeploy?modal=values)

The following pods should be running successfully (status is `Running`) after the default installation (KServe Backend):

- Run `kubectl get pods -n deeploy` to check the Deeploy installation. Example output:
  ```
  NAME                                        READY   STATUS    RESTARTS   AGE
  deeploy-core-856f4c4798-b6jdc               1/1     Running   0          17m
  deeploy-frontend-785678b464-rn6pb           1/1     Running   0          17m
  deeploy-introspection-956569d8f-vwdt8       1/1     Running   0          17m
  deeploy-router-126f4c4798-b6jdc             1/1     Running   0          17m
  deeploy-usage-6f48b7d656-zq27t              1/1     Running   0          17m
  ...
  ```

  Moreover you can check the installation status on `https://<host>/installation-page` if this was enabled in the [Helm chart values](https://artifacthub.io/packages/helm/deeploy-core/deeploy?modal=values)

## Update Deeploy

> We advise to limit updating to 5 minor versions of Deeploy at once (e.g. `1.5.0` to `1.10.0`). Updating more than 5 minor versions may cause problems, since some deprecations might cause unexpected behaviour in your installation.

When updating an existing installation, we advise to follow the steps below:

- Check the [release notes](https://gitlab.com/deeploy-ml/deeploy-install/-/releases) for required and breaking changes.
- Check dependencies and run the [install script](./scripts/install-requirements.sh) again to update dependencies for your Deeploy release.
- Check the [values.yaml file](https://artifacthub.io/packages/helm/deeploy-core/deeploy?modal=values) for your Deeploy release and apply any required changes
- Check if you have added the deeploy Helm repo as in [Step 2. Deeploy Helm chart](#step-2-deeploy-helm-chart), if not execute the steps listed there.
- To execute the update run:

```bash
helm upgrade deeploy deeploy/deeploy -f values.yaml --version 1.45.1 --namespace deeploy
```

## Uninstall Deeploy

To uninstall an installation and clean up all cluster resources run the following script: [./scripts/uninstall-deeploy.sh](./scripts/uninstall-deeploy.sh). This will uninstall the Helm chart and remove all resources created during the pre-install.
