#!/bin/bash

# Versions
export ISTIO_VERSION=1.21.6
export KNATIVE_VERSION=knative-v1.13.1
export KNATIVE_NET_ISTIO_VERSION=knative-v1.13.1
export CERT_MANAGER_VERSION=v1.13.6
export KSERVE_VERSION=v0.12.0  #maintain kserve crd version 12.0 to prevent migration issues

# Optional settings
export ISTIO_INJECTION_ENABLED=false
export KIALI_ENABLED=false
export PROMETHEUS_ENABLED=false
export GRAFANA_ENABLED=false

KUBE_VERSION=$(kubectl version)
KUBE_CUR_CTX=$(kubectl config current-context)

echo "\nThis script will install the required dependencies for Deeploy using the current context: ${KUBE_CUR_CTX}"
echo "The current context has versions: \n${KUBE_VERSION}"
read -p "Press enter to continue with the installation..."

# Install Istio using helm for Knative https://knative.dev/docs/install/installing-istio/#installing-istio-with-sidecar-injection
# Default installation without sidecar injection
helm repo add istio https://istio-release.storage.googleapis.com/charts
helm repo update

kubectl create namespace istio-system

helm install istio-base istio/base --version ${ISTIO_VERSION} -n istio-system
helm install istiod istio/istiod --version ${ISTIO_VERSION} -n istio-system --wait
helm install istio-ingressgateway istio/gateway --version ${ISTIO_VERSION} -n istio-system

# Enable istio-injection in istio-system namespace (optional, default false):
if [ "$ISTIO_INJECTION_ENABLED" = true ]; then kubectl label namespace istio-system istio-injection=enabled; else echo "Sidecar injection for Istio is not enabled"; fi
# Enable kiali addon (optional, default false):
if [ "$KIALI_ENABLED" = true ]; then kubectl apply -f https://raw.githubusercontent.com/istio/istio/release-1.12/samples/addons/kiali.yaml; else echo "Kiali integration for Istio is not enabled"; fi
# Enable prometheus addon (optional, default false):
if [ "$PROMETHEUS_ENABLED" = true ]; then kubectl apply -f https://raw.githubusercontent.com/istio/istio/release-1.12/samples/addons/prometheus.yaml; else echo "Prometheus integration for Istio is not enabled"; fi
# Enable grafana addon (optional, default false):
if [ "$GRAFANA_ENABLED" = true ]; then kubectl apply -f https://raw.githubusercontent.com/istio/istio/release-1.12/samples/addons/grafana.yaml; else echo "Grafana integration for Istio is not enabled"; fi

# Install Knative
kubectl apply -f https://github.com/knative/serving/releases/download/${KNATIVE_VERSION}/serving-crds.yaml
kubectl apply -f https://github.com/knative/serving/releases/download/${KNATIVE_VERSION}/serving-core.yaml
kubectl apply -f https://github.com/knative/serving/releases/download/${KNATIVE_VERSION}/serving-hpa.yaml

# Allow Deeploy to select nodes for serverless kserve deployments
kubectl patch configmap/config-features \
  --namespace knative-serving \
  --type merge \
  --patch '{"data":{"kubernetes.podspec-nodeselector": "enabled"}}'
kubectl apply -f https://github.com/knative/net-istio/releases/download/${KNATIVE_NET_ISTIO_VERSION}/net-istio.yaml
# Create knative local gateway
kubectl patch configmap/config-istio \
  --namespace knative-serving \
  --type merge \
  --patch '{"data":{"local-gateway.knative-serving.knative-local-gateway": "knative-local-gateway.istio-system.svc.cluster.local"}}'
echo $(kubectl --namespace istio-system get service istio-ingressgateway)

# Install Cert-manager
kubectl apply --validate=false -f https://github.com/jetstack/cert-manager/releases/download/${CERT_MANAGER_VERSION}/cert-manager.yaml
kubectl wait --for=condition=available --timeout=600s deployment/cert-manager-webhook -n cert-manager

# Kserve CRDs
helm template oci://ghcr.io/kserve/charts/kserve-crd --version $KSERVE_VERSION > kserve-crds.yaml
kubectl apply -f kserve-crds.yaml -n default
rm kserve-crds.yaml
