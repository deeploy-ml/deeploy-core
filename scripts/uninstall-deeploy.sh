
echo "This script will uninstall the Deeploy helm release on current kubectl context: $(kubectl config current-context)"
read -p "Press enter to continue..."

helm uninstall deeploy -n deeploy
kubectl delete jobs,pods,sa,secrets,clusterroles,clusterrolebinding -l app.kubernetes.io/instance=deeploy -n deeploy